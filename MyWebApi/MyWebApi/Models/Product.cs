﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyWebApi.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public decimal ProductPrice { get; set; }
        [Required]
        public string ProductDescription { get; set; }
        public byte[] ProductImage { get; set; }
    }


    public class ProductFormModel
    {
        public int Id { get; set; }

        [Display(Name = "Product Name")]
        [Required]
        public string ProductName { get; set; }

        [Required]
        [Display(Name = "Product Price")]
        public decimal ProductPrice { get; set; }

        [Required]
        [Display(Name = "Product Description")]
        public string ProductDescription { get; set; }

        [Display(Name = "Product Image")]
        public IFormFile ProductImage { get; set; }
    }


    public class ProductViewModel
    {
        [Display(Name = "Image")]
        public string ImageUrl { get; set; }

        [Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Display(Name = "Description")]
        public string ProductDescription { get; set; }

        [Display(Name = "Price")]
        public decimal ProductPrice { get; set; }
        public int Id { get; set; }
    }



    public class ProductRepository : IProduct
    {
        private readonly ProductContext _dbContext = null;
        public ProductRepository(ProductContext productContext)
        {
            _dbContext = productContext;
        }

        public async Task<bool> AddNewProductAsync(Product product)
        {
            if (product == null)
                return false;
            _dbContext.Add(product);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> RemoveProductAsync(int productId)
        {
            var toDel = _dbContext.Products.FirstOrDefault(x => x.Id == productId);
            if (toDel != null)
            {
                _dbContext.Products.Remove(toDel);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> ModifyProductAsync(Product product)
        {
            await _dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<Product> FindProductAsync(int id)
        {
            return await _dbContext.Products.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Product>> AllProductsAsync()
        {
            return await _dbContext.Products.ToListAsync();
        }
    }



    public interface IProduct
    {
        Task<Product> FindProductAsync(int id);
        Task<List<Product>> AllProductsAsync();
        Task<bool> RemoveProductAsync(int productId);
        Task<bool> AddNewProductAsync(Product product);
        Task<bool> ModifyProductAsync(Product product);
    }

}
