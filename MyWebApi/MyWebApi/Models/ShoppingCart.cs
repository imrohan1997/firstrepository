﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyWebApi.Models
{
    public class Cart
    {

        public int Id { get; set; }
        public int  Qty { get; set; }
        public int ProductId { get; set; }
        public DateTime AddedTime { get; set; }
        
    }
}
