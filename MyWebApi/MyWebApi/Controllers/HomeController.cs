﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyWebApi.Models;
using System.IO;

namespace MyWebApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProduct _product;

        public HomeController(IProduct context)
        {
            _product = context;
        }

       
        public async Task<IActionResult> Index()
        {
            var data = await _product.AllProductsAsync();
            var newData = data.Select(x =>
               new ProductViewModel
               {
                   ImageUrl = Helper.ConvertBinaryToImage(x.ProductImage),
                   Id = x.Id,
                   ProductDescription = x.ProductDescription,
                   ProductName = x.ProductName,
                   ProductPrice = x.ProductPrice
               }
             );
            return View(newData);
        }

        // GET: Home/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var product = await _product.FindProductAsync(id);
            if (product == null)
                return NotFound();

            var productViewModel = new ProductViewModel()
            {
                Id = product.Id,
                ImageUrl = Helper.ConvertBinaryToImage(product.ProductImage),
                ProductDescription = product.ProductDescription,
                ProductName = product.ProductName,
                ProductPrice = product.ProductPrice
            };

            return View(productViewModel);
        }

        // GET: Home/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Home/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductFormModel productForm)
        {
            if (!ModelState.IsValid)
                return View(productForm);

            var files = productForm.ProductImage;
            var product = new Product()
            {
                Id = productForm.Id,
                ProductDescription = productForm.ProductDescription,
                ProductImage = files?.Length > 0 ? Helper.ConvertFileToBinary(files) : null,
                ProductName = productForm.ProductName,
                ProductPrice = productForm.ProductPrice
            };

            await _product.AddNewProductAsync(product);
            return RedirectToAction(nameof(Index));
        }

        // GET: Home/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var product = await _product.FindProductAsync(id);

            if (product == null)
                return NotFound();

            var productViewModel = new ProductViewModel()
            {
                Id = product.Id,
                ProductName = product.ProductName,
                ProductPrice = product.ProductPrice,
                ProductDescription = product.ProductDescription,
                ImageUrl = Helper.ConvertBinaryToImage(product.ProductImage)
            };

            return View(productViewModel);
        }

        // POST: Home/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ProductFormModel product)
        {
            if (!ModelState.IsValid)
                return View(product);


            var dataToUpdate = await _product.FindProductAsync(id);
            if (dataToUpdate == null || product == null)
                return NotFound();

            var file = Request.Form.Files["NewImage"];
            if (file != null && file?.Length > 0)
                dataToUpdate.ProductImage = Helper.ConvertFileToBinary(file);

            dataToUpdate.ProductName = product.ProductName;
            dataToUpdate.ProductPrice = product.ProductPrice;
            dataToUpdate.ProductDescription = product.ProductDescription;

            await _product.ModifyProductAsync(dataToUpdate);

            return RedirectToAction(nameof(Index));

        }

        // GET: Home/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            var product = await _product.FindProductAsync(id);
           
            if (product == null)
                return NotFound();

            var productViewModel = new ProductViewModel()
            {
                Id = product.Id,
                ProductName = product.ProductName,
                ProductPrice = product.ProductPrice,
                ProductDescription = product.ProductDescription,
                ImageUrl = Helper.ConvertBinaryToImage(product.ProductImage)
            };

            return View(productViewModel);
        }

        // POST: Home/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _product.RemoveProductAsync(id);
            return RedirectToAction(nameof(Index));
        }

    }
}
