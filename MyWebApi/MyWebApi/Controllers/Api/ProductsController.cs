﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyWebApi.Models;

namespace MyWebApi.Controllers.Api
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProduct _product;
        public ProductsController(IProduct product)
        {
            _product = product;
        }

        [HttpGet]
        public async Task<IActionResult> GetProducts()
        {
            var data = await _product.AllProductsAsync();
            var newData = data.Select(x =>
               new ProductViewModel
               {
                   ImageUrl = Helper.ConvertBinaryToImage(x.ProductImage),
                   Id = x.Id,
                   ProductDescription = x.ProductDescription,
                   ProductName = x.ProductName,
                   ProductPrice = x.ProductPrice
               }
             );

            return Ok(newData.ToList());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct(int id)
        {
            var product = await _product.FindProductAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            var productVm = new ProductViewModel
            {
                Id = product.Id,
                ImageUrl = Helper.ConvertBinaryToImage(product.ProductImage),
                ProductDescription = product.ProductDescription,
                ProductName = product.ProductName,
                ProductPrice = product.ProductPrice
            };

            return Ok(productVm);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditProduct(int id, [FromForm] ProductFormModel product)
        {

            if (!ModelState.IsValid)
                return ValidationProblem();

            var dataToUpdate = await _product.FindProductAsync(id);
            if (dataToUpdate == null || product == null)
                return NotFound();

            var files = product.ProductImage;
            if (files != null && files?.Length > 0)
                dataToUpdate.ProductImage = Helper.ConvertFileToBinary(files);

            dataToUpdate.ProductName = product.ProductName;
            dataToUpdate.ProductPrice = product.ProductPrice;
            dataToUpdate.ProductDescription = product.ProductDescription;

            return Ok(await _product.ModifyProductAsync(dataToUpdate));
        }

        [HttpPost]
        public async Task<IActionResult> PostProduct([FromForm] ProductFormModel productForm)
        {
            if (!ModelState.IsValid)
                return ValidationProblem();
            try
            {
                var files = productForm.ProductImage;
                var product = new Product()
                {
                    Id = productForm.Id,
                    ProductDescription = productForm.ProductDescription,
                    ProductImage = files?.Length > 0 ? Helper.ConvertFileToBinary(files) : null,
                    ProductPrice = productForm.ProductPrice,
                    ProductName = productForm.ProductName
                };

                await _product.AddNewProductAsync(product);
                return CreatedAtAction("GetProduct", new { id = product.Id }, product);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            return Ok(await _product.RemoveProductAsync(id));
        }
    }
}
