﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using MyWebApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace MyWebApi
{
    public static class Helper
    {
        /// <summary>
        /// Save into binary data
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static byte[] ConvertFileToBinary(IFormFile file)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                file.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public static string ConvertBinaryToImage(byte[] binaryData)
        {
            if (binaryData == null)
                return string.Empty;
            string base64String = Convert.ToBase64String(binaryData);
            return string.Format("data:image/jpg;base64,{0}", base64String);
        }

        public static IFormFile ConvertBinaryToFile(byte[] fileStream)
        {
            if (fileStream == null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    return new FormFile(ms, 0, 0, "", "");
                }
            }

            using (MemoryStream ms = new MemoryStream(fileStream))
            {
                return new FormFile(ms, 0, fileStream.Length, "", "");
            }
        }


    }
}